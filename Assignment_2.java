import java.util.*;
abstract class Shape {
	abstract double area();
}
class Circle extends Shape {
	double radius;
	Circle(double temp_radius){
		this.radius = temp_radius;
	}
	double area(){
		return 2*Math.PI*radius;
	}
}
class Rectangle extends Shape {
	double length,breadth;
	Rectangle(double temp_length, double temp_breadth){
		this.length = temp_length;
		this.breadth = temp_breadth;
	}
	double area(){
		return length*breadth;
	}
}
class Square extends Shape {
	double side;
	Square(double temp_side){
		this.side = temp_side;
	}
	double area(){
		return side*side;
	}
}
class Triangle extends Shape {
	double height,base;
	Triangle(double temp_height , double temp_base){
		this.height = temp_height;
		this.base = temp_base;
	}
	double area(){
		return (1/2)*base*height;
	}
}
public class Assignment_2 {
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		String stop = " ";
		do{
			System.out.println("1.Circle\n2.Rectangle\n3.Square\n4.Triangle");
			String temp = sc.next();
			try{
				int opt = Integer.parseInt(temp);	//NumberFormatException may occur
				switch(opt){
				case 1:
					System.out.print("Radius: ");
					temp = sc.next();
					try{
						double radius = Double.parseDouble(temp);	//NumberFormatException may occur
						Circle circle = new Circle(radius);
						System.out.println(circle.area());
					} catch(NumberFormatException e){
						System.out.println("Invalid format of input : Radius");
					}
					break;
				case 2:
					System.out.print("Length: ");
					temp = sc.next();
					try{
						double length = Double.parseDouble(temp);	//NumberFormatException may occur
						System.out.print("Breadth: ");
						temp = sc.next();
						try{
							double breadth = Double.parseDouble(temp);	//NumberFormatException may occur
							Rectangle rectangle = new Rectangle(length,breadth);
							System.out.println(rectangle.area());
						} catch(NumberFormatException e){
							System.out.println("Invalid format of input : Breadth");
						}
					} catch(NumberFormatException e){
						System.out.println("Invalid format of input : Height");
					}
					break;
				case 3:
					System.out.print("Side: ");
					temp = sc.next();
					try{
						double side = Double.parseDouble(temp);	//NumberFormatException may occur
						Square square = new Square(side);
						System.out.println(square.area());
					} catch(NumberFormatException e){
						System.out.println("Invalid format of input : Side");
					}
					break;
				case 4:
					System.out.print("Height: ");
					temp = sc.next();
					try{
						double height = Double.parseDouble(temp);	//NumberFormatException may occur
						System.out.print("Base: ");
						temp = sc.next();
						try{
							double base = sc.nextDouble();	//NumberFormatException may occur
							Triangle triangle = new Triangle(height,base);
							System.out.println(triangle.area());
						} catch(NumberFormatException e){
							System.out.println("Invalid input : Base");
						}
					} catch(NumberFormatException e){
						System.out.println("Invalid format of input : Height");
					}
					break;
				default: 
					System.out.println("Invalid option selected, Try again.");
					break;
				}
			} catch(NumberFormatException e){
				System.out.println("Invalid format of input");
			}
			System.out.println("Type yes to redo");
			stop = sc.next();
		}while(stop.equals("yes"));
		sc.close();
	}
}